const mongoose = require('mongoose');


module.exports = () => {
	mongoose.connect(process.env.MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true,
		autoIndex: true
	})
		.then(() => console.log('Connected to database'))
		.catch(e => console.log(e));
};