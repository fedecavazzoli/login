const express = require('express');
const Cors = require('cors');
const routes = require('./routes/index');
require('dotenv').config();

const app = express();

require('./startup/db')();

app.use('/api', routes);


app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});