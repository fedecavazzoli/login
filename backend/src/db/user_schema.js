const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, 'A username is required']
	},
	password: {
		type: String,
		// eslint-disable-next-line no-unused-expressions
		required: [function() { this.provider === 'email'; }, 'A password is required']
	},
	email: {
		type: String,
		trim: true,
		lowercase: true,
		index: { unique: true },
		required: [true, 'Email address is required'],
		match: [/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/, 'Please fill a valid email address']
	},
	createdAt: {
		type: Date,
		index: true,
		default: new Date()
	},
	accessToken: {
		type: String
	}
});

module.exports = userSchema;