const mongoose = require('mongoose');

module.exports = class Model {

	constructor(name, schema) {
		this.model = mongoose.model(name, schema);
	}

  async findBy(field, value) {
		const items = await this.model.find({ [field]: value });
		return items.length ? items[0]._doc : null;
	}

  async create(data) {
		const { _doc } = await this.model.create(data);
		return _doc;
	}
};
