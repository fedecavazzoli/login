const { userServices, jwt } = require('../services/index')

const signUp = async (req, res) => {
  const userData = req.body;

  try{
    const { password, ...user } = await userServices.createUser(userData);
    res.status(201).send( user );
	} catch(error) {
		return res.status(400).send({ message: error.message });
	}
};

const signIn = async (req, res) => {
  const { email, password: passwordToValidate } = req.body;
  const user = await userServices.getUserByEmail(email);

  if(!user)
    return res.status(400).send({ message: 'Invalid email' });

  const { password, ...formattedUser } = user;

  const areCredentialsValid = await userServices.validateCredentials(passwordToValidate, password);

  if(!areCredentialsValid)
    return res.status(401).send({ message: 'Invalid email or password' });

  const token = jwt.generate(formattedUser);

  res.header('Authorization', token).status(201).send({ accessToken: token, ...formattedUser });
  
};

const logout = async (req, res) => {
  res.send('loguot');
};

module.exports = {
  signUp, signIn, logout
};