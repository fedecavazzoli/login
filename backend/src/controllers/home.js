const { userServices } = require('../services/index')

const index = async (req, res) => {
  const { email } = req.userData;
  const user = await userServices.getUserByEmail(email);

  res.status(200).send({ message: `Hello: ${user.username}` });
};

module.exports = { index };