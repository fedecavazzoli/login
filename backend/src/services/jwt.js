const jwt = require('jsonwebtoken');

const generate = ({ email, _id }) => {
  const payload = { email, _id };
  const secret = process.env.JWT_SECRET;
  return jwt.sign(payload, secret);
};

const verify = token => {
	return jwt.verify(token, process.env.JWT_SECRET);	
};

module.exports = { generate, verify };