const Bcrypt = require('bcrypt');
const userSchema = require('../db/user_schema');
const Model = require('../db/model');
const userModel = new Model('Users', userSchema);

const getEncriptedPassword = async password => {
	const salt = await Bcrypt.genSalt(10);
	return Bcrypt.hash(password, salt);
};

const createUser = async user => {
	const encriptedPassword = await getEncriptedPassword(user.password);
	const newUser = await userModel.create({ ...user, password: encriptedPassword });
	return newUser;
};

const getUserByEmail = email => {
	return userModel.findBy('email', email);
};

const validateCredentials = (passwordToValidate, password) => {
	return Bcrypt.compare(passwordToValidate, password);
};

module.exports = {
  createUser,
  getUserByEmail,
	validateCredentials
}