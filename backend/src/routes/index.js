const { users, home } = require('../controllers/index');
const authenticateUser = require('../middleware/authenticate_user')

const express = require('express');
const bodyParser = require('body-parser');

const router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));


router.post('/users/sign_up', users.signUp);
router.post('/users/sign_in', users.signIn);
router.delete('/users/logout', users.logout);
router.get('/', authenticateUser, home.index);

module.exports = router;