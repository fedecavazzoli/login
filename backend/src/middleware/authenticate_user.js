const { jwt } = require('./../services/index');

module.exports = async (req, res, next) => {
	const token = req.header('Authorization');

	if(!token)
		return res.status(401).send({ message: 'Access denied. No token provided.' });

	try {
		const payload = jwt.verify(token);
		req.userData = payload;
		next();
	} catch(err) {
		res.status(401).send({ message: err.message });
	}
};